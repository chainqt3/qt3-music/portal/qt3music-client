### Nginx

```
sudo ln -s /etc/nginx/sites-available/{file.se} /etc/nginx/sites-enabled/
sudo certbot --nginx -d exmample.se -d www.example.se
sudo systemctl restart nginx
sudo systemctl status nginx
```

### Config

```
server {
    server_name http://localhost:1000;

    location / {
    proxy_pass http://192.168.240.1:3000;
    }

}

server {
    server_name localhost:3000;
    listen 80;
}


```

```
   proxy_pass http://gt3music-client:3000;
```
