import Keycloak from "keycloak-js";

const settings = {
    realm: "qt3music",
    clientId: "qt3music-client",
    url: "https://auth.chaincuet.com/auth/",
    sslRequired : "external",
    publicClient: true,
    verifyTokenAudience: true,
    useResourceRoleMappings: true,
    confidentialPort: 0
}

const keycloak = new Keycloak(settings);

export default keycloak;
