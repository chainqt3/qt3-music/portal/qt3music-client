import lightThemeColors from "./utils/lightThemeColors";

const {info, grey} = lightThemeColors;

const globals = {
    "*, *::before, *::after": {
        margin: 0,
        padding: 0,
    },
    body: {
        fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue, sans-serif'",
        webkitFontSmoothing: "antialiased",
        mozOsxFontSmoothing: "grayscale",
    },
    code: {
        fontFamily: "source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace",
    },
    '*::-webkit-scrollbar': {
        width: '5px',
    },
    '*::-webkit-scrollbar-thumb': {
        backgroundColor: 'rgba(5,40,146,0.55)',
        borderRadius: 100,
    },
    '::-webkit-scrollbar-thumb:hover': {
         backgroundColor: "#052892",
    }
};

export default globals;
