import * as React from 'react';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import {green} from '@mui/material/colors';
import Button from '@mui/material/Button';
import Fab from '@mui/material/Fab';
import CheckIcon from '@mui/icons-material/Check';
import SaveIcon from '@mui/icons-material/Save';
import ElectricBoltIcon from "@mui/icons-material/ElectricBolt";
import Typography from "@mui/material/Typography";
import {CardActionArea, Grid, ToggleButton} from '@mui/material';

export default function Logo() {
    const [loading, setLoading] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    const timer = React.useRef<number>();

    const buttonSx = {
        backgroundColor: "#fff",
        ...(success && {
            bgcolor: "#fff",
            '&:hover': {
                bgcolor: "#fff",
            },
        }),
    };

    React.useEffect(() => {
        return () => {
            clearTimeout(timer.current);
        };
    }, []);

    const handleButtonClick = () => {
        if (!loading) {
            setSuccess(false);
            setLoading(true);
            timer.current = window.setTimeout(() => {
                setSuccess(true);
                setLoading(false);
            }, 500);
        }
    };

    return (
        <Box>
            <CardActionArea sx={{padding: 2}}>
                <Grid container>
                    <Grid item>
                        < ElectricBoltIcon/>
                    </Grid>
                    <Grid item>
                        <Typography ml={1} variant={"h6"}>QT3Music</Typography>
                    </Grid>
                </Grid>
            </CardActionArea>
        </Box>
    )
}