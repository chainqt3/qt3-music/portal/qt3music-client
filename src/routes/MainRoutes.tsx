import React from 'react';
import {Navigate, Outlet, Route, Routes} from "react-router-dom";
import LoginStyle from "../root/LoginStyle";
import Page404NotFound from "../utils/Page404NotFound";
import ProtectedRoute from "./ProtectedRoute";
import RenderOnAuthenticated from "../utils/RenderOnAuthenticated";
import MainStyle from "../root/MainStyle";
import HomeView from "../views/home/HomeView";
import LikedSongsView from "../views/liked-songs/LikedSongsView";
import SearchView from "../views/search/SearchView";
import {useKeycloak} from "@react-keycloak/web";
import LoadingProgress from "../utils/LoadingProgress";

/* ROUTE LINKS */
export const LinkToHomeView = () => "/home";
export const LinkToSearchView = () => "/search";
export const LinkToLikedSongsView = () => "/liked-songs";
export const LinkTo404NotFound = () => "404";
/* ROUTE LINKS */

export default function MainRouter() {
    const {keycloak} = useKeycloak();
    const token = keycloak && keycloak.token;
    console.log(token)

    // if (!keycloak.authenticated) {
    //     keycloak.login();
    // }

    return (
        <Routes>
            <Route path={"/"} element={
                // <RenderOnAuthenticated>
                    <MainStyle/>
                // </RenderOnAuthenticated>
            }>
                <Route path={"/"} element={<HomeView/>}/>
                <Route path={LinkToHomeView()} element={<HomeView/>}/>
                <Route path={LinkToLikedSongsView()} element={<LikedSongsView/>}/>
                <Route path={LinkToSearchView()} element={<SearchView/>}/>
            </Route>
            {/*<Route path={"/"} element={<LoginStyle/>}>*/}
            {/*    <Route path={LinkToLoginView()} element={!keycloak.authenticated ? <LoginView/> :*/}
            {/*        <Navigate to={LinkToOverviewView()} replace/>}/>*/}
            {/*    <Route path={LinkTo404NotFound()} element={<Page404NotFound/>}/>*/}
            {/*    <Route path="/" element={<Navigate to={LinkToLoginView()}/>}/>*/}
            {/*    <Route path="*" element={<Navigate to={LinkTo404NotFound()}/>}/>*/}
            {/*</Route>*/}
        </Routes>

    )
}
