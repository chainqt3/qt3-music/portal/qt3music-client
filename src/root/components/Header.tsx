import * as React from 'react';
import {FC, useState} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import {Button, Grid, Tooltip} from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";
import useTheme from "@mui/material/styles/useTheme";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import ProfileIcon from "../../assets/images/small-logos/qt3icon.jpg"
import LaunchIcon from '@mui/icons-material/Launch';
import Dropdown from 'react-bootstrap/Dropdown';
import {useKeycloak} from "@react-keycloak/web";
import { headerFooterPadding } from '../MainStyle';

interface DrawerMenuI {
    onMenuClick: () => void,
}

const Header: FC<DrawerMenuI> = (props) => {
    let theme = useTheme();
    let {keycloak} = useKeycloak();
    const isMobileSize = useMediaQuery(theme.breakpoints.down('md'));

    return (
        <AppBar position={"fixed"} sx={{ paddingLeft: {md: `calc(${headerFooterPadding}px)`}}}>
            <Toolbar>
                <Grid container justifyContent={"end"}>
                    <Grid item mr={2}>
                        <Dropdown>
                            <Dropdown.Toggle variant={"dark"} style={{
                                display: "flex",
                                borderRadius: 100,
                                width: "200px",
                                flexDirection: "row",
                                alignItems: "center",

                            }}>
                                <Grid container>
                                    <Avatar
                                        alt="Remy Sharp"
                                        src={ProfileIcon}
                                        sx={{width: 24, height: 24}}
                                    />
                                    <Typography ml={1}>John Doe</Typography>
                                </Grid>
                            </Dropdown.Toggle>
                            <Dropdown.Menu variant="dark" style={{width: "200px", padding: 5}}>
                                <Dropdown.Item href="#/action-1" style={{padding: 10}}>
                                    <Grid container>
                                        <Typography>Account</Typography>
                                        <Box flexGrow={1}/>
                                        <LaunchIcon/>
                                    </Grid>
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-2" style={{padding: 10}}>
                                    <Grid container>
                                        <Typography>Profile</Typography>
                                    </Grid>
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-3" style={{padding: 10}}>
                                    <Grid container>
                                        <Typography>Settings</Typography>
                                    </Grid>
                                </Dropdown.Item>
                                <Dropdown.Item href="https://chaincuet.com/" style={{padding: 10}}>
                                    <Grid container>
                                        <Typography>Chaincue Technologies</Typography>
                                    </Grid>
                                </Dropdown.Item>
                                <Dropdown.Divider/>
                                <Dropdown.Item href="#/action-4" style={{padding: 10}} onClick={() => keycloak.logout()}>
                                    <Grid container>
                                        <Typography>Log out</Typography>
                                    </Grid>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    )
}

export default Header;
