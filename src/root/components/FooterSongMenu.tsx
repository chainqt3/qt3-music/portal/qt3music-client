import * as React from 'react';
import {useEffect} from 'react';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import {Card, CardActionArea, CardMedia, Grid, Paper, Slider, Stack} from "@mui/material";
import SongProgressBar from "./SongProgressBar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import AlbumImage from "../../assets/images/billing-background-card.png"
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import SkipNextIcon from '@mui/icons-material/SkipNext';
import ShuffleIcon from '@mui/icons-material/Shuffle';
import RepeatIcon from '@mui/icons-material/Repeat';
import LyricsIcon from '@mui/icons-material/Lyrics';
import QueueMusicIcon from '@mui/icons-material/QueueMusic';
import ConnectedTvIcon from '@mui/icons-material/ConnectedTv';
import VolumeUp from '@mui/icons-material/VolumeUp';
import useMediaQuery from "@mui/material/useMediaQuery";
import useTheme from "@mui/material/styles/useTheme";
import VolumeDown from "@mui/icons-material/VolumeDown";

export default function FooterSongMenu() {
    let theme = useTheme();
    const isMobileSize = useMediaQuery(theme.breakpoints.down('md'));
    const [value, setValue] = React.useState(0);
    const [progress, setProgress] = React.useState(0);
    const [buffer, setBuffer] = React.useState(10);

    const handleChange = (event: Event, newValue: number | number[]) => {
        setValue(newValue as number);
    };

    const progressRef = React.useRef(() => {
    });
    useEffect(() => {
        const timer = setInterval(() => {
            setProgress((oldProgress) => {
                if (oldProgress === 100) {
                    return 0;
                }
                return oldProgress + 0.1
            });
        }, 50);

        return () => {
            clearInterval(timer);
        };
    }, []);

    React.useEffect(() => {
        const timer = setInterval(() => {
            progressRef.current();
        }, 500);

        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <Paper sx={{marginTop: 0}} elevation={3}>
            <Box>
                <Grid container>
                    <Grid item xs={12}>
                        <Grid container justifyContent={"space-between"}>
                            <Grid item xs={4} minHeight={"100%"}>
                                <Grid container alignItems={"center"}>
                                    <Card sx={{maxWidth: 100, borderRadius: 0}} >
                                        <CardActionArea>
                                            <CardMedia
                                                component="img"
                                                image={AlbumImage}
                                                alt="green iguana"
                                            />
                                        </CardActionArea>
                                    </Card>
                                    <Typography ml={1}>Song name</Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={4}>
                                <Grid container justifyContent={"center"}>
                                    <Grid item hidden={isMobileSize}>
                                        <IconButton color="secondary" aria-label="add an alarm">
                                            <ShuffleIcon/>
                                        </IconButton>
                                        <IconButton color="secondary" aria-label="add an alarm">
                                            <SkipPreviousIcon/>
                                        </IconButton>
                                        <IconButton color="secondary" aria-label="add an alarm">
                                            <PlayArrowIcon fontSize={"large"}/>
                                        </IconButton>
                                        <IconButton color="secondary" aria-label="add an alarm">
                                            <SkipNextIcon/>
                                        </IconButton>
                                        <IconButton color="secondary" aria-label="add an alarm">
                                            <RepeatIcon/>
                                        </IconButton>
                                    </Grid>
                                    <Grid item hidden={!isMobileSize}>
                                        <IconButton color="secondary" aria-label="add an alarm">
                                            <PlayArrowIcon fontSize={"large"}/>
                                        </IconButton>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item hidden={isMobileSize} xs={4}>
                                <Grid container alignItems={"center"}>
                                    <Grid item xs={8} hidden={isMobileSize}>
                                        <Grid container justifyContent={"end"}>
                                            <IconButton color="secondary" aria-label="add an alarm">
                                                <LyricsIcon/>
                                            </IconButton>
                                            <IconButton color="secondary" aria-label="add an alarm">
                                                <QueueMusicIcon/>
                                            </IconButton>
                                            <IconButton color="secondary" aria-label="add an alarm">
                                                <ConnectedTvIcon/>
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Grid container alignItems={"center"}>
                                            <Box sx={{width: 200}}>
                                                <Stack spacing={1} direction="row" alignItems="center">
                                                    <IconButton color="secondary">
                                                        <VolumeDown/>
                                                    </IconButton>
                                                    <Slider aria-label="Volume" value={value}
                                                            onChange={handleChange}/>
                                                    <IconButton color="secondary">
                                                        <VolumeUp/>
                                                    </IconButton>
                                                </Stack>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <SongProgressBar/>
                    </Grid>
                </Grid>
            </Box>
        </Paper>
    )
}
