import React, {useContext, useEffect, useState} from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import {Playlist, RootModel} from "../RootModel";
import {RootModelContext} from "../RootModelContext";
import {CardActionArea, Divider, Grid, ListItem, ListItemButton, ListItemText, SvgIconTypeMap} from '@mui/material';
import useMediaQuery from "@mui/material/useMediaQuery";
import useTheme from "@mui/material/styles/useTheme";
import ElectricBoltIcon from "@mui/icons-material/ElectricBolt";
import {LinkToHomeView, LinkToLikedSongsView, LinkToSearchView} from "../../routes/MainRoutes";
import {Link, useLocation} from "react-router-dom";
import darkThemeColors from "../../assets/themes/utils/darkThemeColors";
import HomeIcon from '@mui/icons-material/Home';
import SearchIcon from '@mui/icons-material/Search';
import FavoriteIcon from '@mui/icons-material/Favorite';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import {OverridableComponent} from "@mui/material/OverridableComponent";

interface Props {
    isOpen?: boolean,
    onClose?: () => void
}

interface SideMenuListItemPROPS {
    text: string,
    routerLink: string
    icon: OverridableComponent<SvgIconTypeMap>
}

export const SideMenuListItem = (props: SideMenuListItemPROPS) => {
    let location = useLocation();
/*
*     const location = useLocation();

    const calculateSelectedColor = () => {
        if (location.pathname.includes(props.routerLink) && props.routerLink !== "/") return true
        return props.routerLink === location.pathname;
    }*/
    return (
        <ListItemButton
            sx={{padding: 2, ":hover": {backgroundColor: darkThemeColors.background.paper},}}
            component={Link}
            to={props.routerLink}
            disableTouchRipple={true}
        >
            <props.icon/>
            <Typography
                fontWeight={location.pathname === props.routerLink ? "bolder" : "normal"}
                ml={1}
                sx={{
                    color: location.pathname === props.routerLink ? darkThemeColors.text.primary : darkThemeColors.text.secondary,
                    fontSize: location.pathname === props.routerLink ? "16px" : "15px",
                }}>
                {props.text}
            </Typography>
        </ListItemButton>
    )
}


const SideMenu = (props: Props) => {
    const theme = useTheme();
    const location = useLocation();
    const rootData = useContext<RootModel>(RootModelContext)
    const toggleDrawerVariant = useMediaQuery(theme.breakpoints.down('md'));
    const drawerWidth = 240;
    const [playlist, setPlaylist] = useState<Playlist[]>([]);

    useEffect(() => {
        setPlaylist(rootData.playlist)
    }, [])

    const btnAddPlaylistHandler = () => {
        const newElement = {id: playlist.length + 1, title: "Playlist" + playlist.length, checked: false}
        setPlaylist(oldArray => [...oldArray, newElement]);
    };

    const updateCheckedStatus = (todoItem: any) => () => {
        setPlaylist(todoList => (todoList.map(item => item.id === todoItem.id ? {
            ...item,
            checked: !item.checked
        } : item)))
    };

    return (
        <Box sx={{width: {md: drawerWidth}, flexShrink: {sm: 0}}}>
            <Drawer
                open={props.isOpen}
                onClose={props.onClose}
                variant={toggleDrawerVariant ? "temporary" : "permanent"}
                ModalProps={{keepMounted: true}}
                sx={{
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                        height: "100vh"
                    },
                    '& > div': {marginTop: {xs: 0, md: 0}}
                }}
            >
                <Grid container p={1}>
                    <Grid item xs={12}>
                        <Box overflow={"auto"} maxHeight={"85vh"}>
                            <CardActionArea sx={{padding: 2}}
                                            component={Link}
                                            to={LinkToHomeView()}
                            >
                                <Grid container>
                                    <ElectricBoltIcon/>
                                    <Typography ml={1} variant={"h6"}>QT3Music</Typography>
                                </Grid>
                            </CardActionArea>
                            <List>
                                {/*{SideMenuListItem(location)}*/}
                                <SideMenuListItem text={"Home"} routerLink={LinkToHomeView()} icon={HomeIcon}/>
                                <SideMenuListItem text={"Search"} routerLink={LinkToSearchView()} icon={SearchIcon}/>
                                <SideMenuListItem text={"Liked Songs"} routerLink={LinkToLikedSongsView()}
                                                  icon={FavoriteIcon}/>
                                <ListItemButton
                                    onClick={btnAddPlaylistHandler}
                                    sx={{padding: 2, ":hover": {backgroundColor: darkThemeColors.background.paper},}}
                                >
                                    <PlaylistAddIcon/>
                                    <Typography ml={1}
                                    >
                                        Create Playlist
                                    </Typography>
                                </ListItemButton>
                            </List>
                            <Divider/>
                            <Grid item>
                                <List sx={{width: '100%', maxWidth: 360}}>
                                    {playlist.map(item => {
                                        return (
                                            <ListItem
                                                key={item.id}
                                                disablePadding
                                            >
                                                <ListItemButton sx={{padding: 0}} onClick={updateCheckedStatus(item)}>
                                                    <ListItemText sx={{paddingLeft: 2}} primary={item.title}/>
                                                </ListItemButton>
                                            </ListItem>
                                        );
                                    })}
                                </List>
                            </Grid>
                        </Box>
                    </Grid>
                </Grid>
            </Drawer>
        </Box>
    );
}

export default SideMenu;
