import * as React from 'react';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import RestoreIcon from '@mui/icons-material/Restore';
import FavoriteIcon from '@mui/icons-material/Favorite';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import {Paper} from "@mui/material";
import {Link} from "react-router-dom";
import {LinkToHomeView, LinkToLikedSongsView, LinkToSearchView} from "../../routes/MainRoutes";
import HomeIcon from '@mui/icons-material/Home';
import SearchIcon from '@mui/icons-material/Search';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';

export default function FooterMobileNav() {
    const [value, setValue] = React.useState(0);

    return (
        <Paper sx={{marginTop: 0,}} elevation={3}>
            <Box sx={{}}>
                <BottomNavigation
                    showLabels
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                >
                    <BottomNavigationAction
                        component={Link}
                        to={LinkToHomeView()}
                        label="Home" icon={<HomeIcon/>}
                    />
                    <BottomNavigationAction
                        component={Link}
                        to={LinkToSearchView()}
                        label="Search" icon={<SearchIcon/>}/>
                    <BottomNavigationAction
                        component={Link}
                        to={LinkToLikedSongsView()}
                        label="Liked Songs" icon={<FavoriteIcon/>}/>
                </BottomNavigation>
            </Box>
        </Paper>

    );
}
