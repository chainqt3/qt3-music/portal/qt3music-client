import * as React from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import {useEffect} from "react";
import {Stack} from '@mui/material';

export default function SongProgressBar() {
    const [progress, setProgress] = React.useState(0);
    const [buffer, setBuffer] = React.useState(10);

    const progressRef = React.useRef(() => {
    });
    useEffect(() => {
        const timer = setInterval(() => {
            setProgress((oldProgress) => {
                if (oldProgress === 100) {
                    return 0;
                }
                return oldProgress + 0.1
            });
        }, 50);

        return () => {
            clearInterval(timer);
        };
    }, []);

    React.useEffect(() => {
        const timer = setInterval(() => {
            progressRef.current();
        }, 500);

        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <Box sx={{width: '100%', color: 'darkgrey'}}>
            <LinearProgress variant="determinate" value={progress} color={"inherit"}/>
        </Box>
    );
}