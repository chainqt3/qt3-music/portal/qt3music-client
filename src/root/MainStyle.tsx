import Header from "./components/Header";
import SideMenu from "./components/SideMenu";
import React, {useState} from "react";
import {Outlet} from "react-router-dom";
import {Box} from "@mui/system";
import {Grid} from "@mui/material";
import Toolbar from "@mui/material/Toolbar";
import FooterMobileNav from "./components/FooterMobileNav";
import ResponsiveAppBar from "./components/PrimarySearchAppBar";
import useMediaQuery from "@mui/material/useMediaQuery";
import useTheme from "@mui/material/styles/useTheme";
import FooterSongMenu from "./components/FooterSongMenu";
import IconButton from "@mui/material/IconButton";
import AppBar from "@mui/material/AppBar";

export const drawerWidth = 250
export const headerFooterPadding = 240

export default function MainStyle() {
    let theme = useTheme();
    const [isOpen, setIsOpen] = useState(false);

    const isMobileSize = useMediaQuery(theme.breakpoints.down('md'));

    return (
        <Box sx={{display: 'flex'}}>
            <Header onMenuClick={() => setIsOpen(!isOpen)}/>
            <SideMenu isOpen={isOpen} onClose={() => setIsOpen(false)}/>
            <Box sx={{flexGrow: 1, p: 3, width: {xs: `calc(100% - ${drawerWidth}px)`}, marginBottom: isMobileSize ? 15 : 10}}>
                <Grid visibility={"hidden"}><Toolbar/></Grid>
                <Box sx={{minHeight: "85vh"}}>
                    <Outlet/>
                </Box>
            </Box>
            <AppBar position="fixed" color="primary" sx={{top: 'auto', bottom: 0, paddingLeft: {md: `calc(${headerFooterPadding}px)`}}}>
                <Box>
                    <FooterSongMenu/>
                </Box>
                <Box hidden={!isMobileSize}>
                    <FooterMobileNav/>
                </Box>
            </AppBar>
        </Box>
    );
}