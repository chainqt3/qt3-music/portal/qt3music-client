import React, {useEffect, useState} from 'react';
import {BrowserRouter, useNavigate} from "react-router-dom";
import MainRoutes from "../routes/MainRoutes";
import {CssBaseline} from "@mui/material";
import {RootModelProvider} from "./RootModelContext";
import {useKeycloak} from "@react-keycloak/web";

export default function Root() {
    return (
        <RootModelProvider>
            <BrowserRouter>
                <CssBaseline/>
                <MainRoutes/>
            </BrowserRouter>
        </RootModelProvider>
    )
}
