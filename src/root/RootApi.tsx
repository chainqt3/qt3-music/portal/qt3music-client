import {RootModel, User} from "./RootModel";

const port = process.env.REACT_APP_BACKEND_BASE_URL;

export const FETCH_ROOT = "FETCH_ROOT";

const fetchRoot = (token: string): Promise<RootModel> => {
    return fetch(`${port}/user/all-users`, {
        headers: {
            Authorization: "Bearer " + token,
        },
    })
        .then(res => res.json());
}

const fetchGatewayTest = (token: string): Promise<string> => {
    return fetch(`${port}/test`, {
        headers: {
            Authorization: "Bearer " + token,
        },
    })
        .then(res => res.text());
}

const fetchUsers = (token: string): Promise<User[]> => {
    return fetch(`${port}/user/all-users`, {
        headers: {
            Authorization: "Bearer " + token,
        },
    })
        .then(res => res.json());
}

export {
    fetchRoot,
    fetchGatewayTest,
    fetchUsers
}
