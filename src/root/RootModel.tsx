import React from 'react';

export const defaultRootData = {
    name: "",
    sideMenuContent: [{id: "", title: "", subContent: [{id: "", title: "",}]}],
    messages: [{id: "", title: "", content: "",}],
    notifyMessages: [{id: "", date: "", time: "", title: "", description: "", author: "",}],
    playlist: [{id: 0, title: "", checked: false,}],
    users: [{id: "", firstname: "", lastname: "", gender: "", email: "",}],
}

export interface RootModel {
    name: string;
    sideMenuContent: SideMenuContent[];
    messages: Messages[];
    notifyMessages: NotifyMessages[];
    playlist: Playlist[]
    users: User[]
}

export interface SideMenuContent {
    id: string;
    title: string;
    subContent: SubContent[];
}

export interface SubContent {
    id: string;
    title: string;
}


export interface Messages {
    id: string;
    title: string;
    content: string;
}

export interface NotifyMessages {
    id: string;
    date: string;
    time: string
    title: string;
    description: string;
    author: string;
}

export interface Playlist {
    id: number;
    title: string;
    checked: boolean;
}

export interface User {
    id: string
    firstname: string
    lastname: string
    gender: string
    email: string
}
