import React from 'react';
import './App.css';
import {useKeycloak} from "@react-keycloak/web";
import {ThemeProvider} from "@mui/material";
import mainTheme from "./assets/themes/mainTheme";
import {QueryClient, QueryClientProvider} from '@tanstack/react-query';
import Root from "./root/Root";
import LoadingProgress from "./utils/LoadingProgress";
import chain from './assets/images/logos/chainicon.png'
import chainWhite from './assets/images/logos/chaincue-logo-crop1-white.png'
import chainDark from './assets/images/logos/chaincue-logo-crop1-dark.png'
import DefaultPage from "./utils/DefaultPage";

const queryClient = new QueryClient()

const App = () => {
  let {keycloak, initialized} = useKeycloak();

  if (!initialized) return <DefaultPage/>

  if (!keycloak.authenticated) {
    keycloak.login();
    return <DefaultPage/>
  }

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={mainTheme}>
        <Root/>
      </ThemeProvider>
    </QueryClientProvider>
  );
};

export default App;
