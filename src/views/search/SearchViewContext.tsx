import React, {createContext, useEffect, useState} from 'react';
import {RootModel} from "../../root/RootModel";
import SearchView from "./SearchView";
import {SearchViewModel} from "./SearchViewModel";
import {SearchViewMock} from "../../mocks/SearchViewMock";

export const SearchViewContext = createContext<SearchViewModel>({} as SearchViewModel);

type Props = {
    children: JSX.Element,
};

export const SearchViewProvider = ({children}: Props) => {
    const [SearchViewModel, setSearchViewModel] = useState<SearchViewModel>()

    useEffect(() => {
        setSearchViewModel(SearchViewMock)
    }, [])

    if (!SearchViewModel) return null

    return (
        <SearchViewContext.Provider value={SearchViewModel}>
            {children}
        </SearchViewContext.Provider>
    )
}
