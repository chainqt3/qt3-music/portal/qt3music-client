import React, {useContext} from 'react';
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {CardActionArea, CardMedia, Grid} from "@mui/material";
import AlbumImage from "../../../assets/images/billing-background-card.png";
import {SearchViewContext} from "../SearchViewContext";
import darkThemeColors from "../../../assets/themes/utils/darkThemeColors";

const BrowseAll = () => {
    const searchViewModel = useContext(SearchViewContext);

    return (
        <Box>
            <Grid container direction={"column"} spacing={3}>
                <Grid item>
                    <Typography mt={5} fontWeight={"bold"} sx={{color: "white"}} variant={"h5"}>
                        Browse All
                    </Typography>
                </Grid>
                <Grid item>
                    <Grid container direction={"row"} spacing={2}>
                        {searchViewModel.genres.map(item => {
                            return (
                                <Grid item key={item.id} xs={6} md={1}>
                                    <CardActionArea sx={{backgroundColor: darkThemeColors.background.paper, borderRadius: 2}}>
                                        <Grid container direction={"column"}>
                                            <Grid item border={"solid"}>
                                                <CardMedia
                                                    component="img"
                                                    image={AlbumImage}
                                                    alt="green iguana"
                                                    sx={{height: 100}}
                                                />
                                            </Grid>
                                            <Grid item>
                                                <Typography variant={"h5"}>{item.name}</Typography>
                                            </Grid>
                                        </Grid>
                                    </CardActionArea>
                                </Grid>
                            )
                        })}
                    </Grid>
                </Grid>
            </Grid>

        </Box>
    );
};

export default BrowseAll;