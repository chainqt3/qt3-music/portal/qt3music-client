import React from 'react';
import Box from "@mui/material/Box";
import {Grid} from "@mui/material";
import RecentSearches from "./RecentSearches";
import BrowseAll from "./BrowseAll";

const Search = () => {
    return (
        <Box>
            <Grid container>
               <Grid item>
                   <RecentSearches/>
               </Grid>
                <Grid item>
                    <BrowseAll/>
                </Grid>
            </Grid>
        </Box>
    );
};

export default Search;