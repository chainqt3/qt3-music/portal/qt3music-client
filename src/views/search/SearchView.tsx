import React from 'react';
import Page from "../../root/components/Page";
import {SearchViewProvider} from "./SearchViewContext";
import Search from "./components/Search";

const SearchView = () => {
    return (
        <Page title={"LikedSongs | ChainQT3"}>
            <SearchViewProvider>
                <Search/>
            </SearchViewProvider>
        </Page>
    );
};

export default SearchView;