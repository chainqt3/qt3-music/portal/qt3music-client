class SearchViewModel {
    id: string;
    name: string;
    recentSearches: RecentSearch[]
    genres: Genre[]

    constructor(id: string, name: string, recentSearches: RecentSearch[], genres: Genre[]) {
        this.id = id;
        this.name = name;
        this.recentSearches = recentSearches;
        this.genres = genres;
    }
}

class RecentSearch {
    id: string;
    name: string;
    genre: string;

    constructor(id: string, name: string, genre: string) {
        this.id = id;
        this.name = name;
        this.genre = genre;
    }
}

class Genre {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

export {
    SearchViewModel,
    RecentSearch,
    Genre
}
