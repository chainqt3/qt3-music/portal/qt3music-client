import React, {createContext, useEffect, useState} from 'react';
import {RootModel} from "../../root/RootModel";
import {HomeViewModel} from "./HomeModel";

export const HomeViewContext = createContext<HomeViewModel>({} as HomeViewModel);

type Props = {
    children: JSX.Element,
};

export const HomeViewProvider = ({children}: Props) => {
    const [homeViewModel, setHomeViewModel] = useState<HomeViewModel>()

    useEffect(() => {
        const tmpArray = {
            id: "1",
            name: "Kristoffer",
            albums: [
                {id: "1", name: "Libertalia", genre: "Rock",},
                {id: "2", name: "Libertalia", genre: "Pop",},
                {id: "3", name: "Libertalia", genre: "Blues",},
                {id: "4", name: "Libertalia", genre: "House",},
                {id: "5", name: "Libertalia", genre: "Punk",},
                {id: "6", name: "Libertalia", genre: "Rock",},
            ],
            yourShows: [
                {id: "1", title: "show1", subTitle: "Description",},
                {id: "2", title: "show2", subTitle: "Description",},
                {id: "3", title: "show3", subTitle: "Description",},
                {id: "4", title: "show4", subTitle: "Description",},
                {id: "5", title: "show5", subTitle: "Description",},
                {id: "6", title: "show6", subTitle: "Description",},
                {id: "7", title: "show7", subTitle: "Description",},
                {id: "8", title: "show8", subTitle: "Description",},
            ]
        }
        setHomeViewModel(tmpArray)
    }, [])

    if (!homeViewModel) return null

    return (
        <HomeViewContext.Provider value={homeViewModel}>
            {children}
        </HomeViewContext.Provider>
    )
}
