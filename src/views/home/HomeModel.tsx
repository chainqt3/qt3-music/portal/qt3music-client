class HomeViewModel {
    id: string;
    name: string;
    albums: Album[];
    yourShows: YourShows[]

    constructor(id: string, name: string, albums: Album[], yourShows: YourShows[]) {
        this.id = id;
        this.name = name;
        this.albums = albums;
        this.yourShows = yourShows;
    }
}

class Album {
    id: string;
    name: string;
    genre: string;

    constructor(id: string, name: string, genre: string) {
        this.id = id;
        this.name = name;
        this.genre = genre;
    }
}

class YourShows {
    id: string;
    title: string;
    subTitle: string;

    constructor(id: string, title: string, subTitle: string) {
        this.id = id;
        this.title = title;
        this.subTitle = subTitle;
    }
}

export {
    HomeViewModel,
    Album
}
