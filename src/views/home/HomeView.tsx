import React from 'react';
import Home from "./components/Home";
import {HomeViewProvider} from "./HomeViewContext";
import Page from "../../root/components/Page";

const HomeView = () => {

    return (
        <Page title={"LikedSongs | ChainQT3"}>
            <HomeViewProvider>
                <Home/>
            </HomeViewProvider>
        </Page>
    )
};

export default HomeView;