import React, {useContext} from 'react';
import {Card, CardActionArea, CardMedia, Grid} from "@mui/material";
import AlbumImage from "../../../assets/images/billing-background-card.png";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import Box from "@mui/material/Box";
import {HomeViewContext} from "../HomeViewContext";
import {Album} from "../HomeModel";

interface Props {
    album: Album
}

const RecentArtists = () => {
    const homeViewModel = useContext(HomeViewContext);

    return (
        <Box>
            <Grid container spacing={1}>
                {homeViewModel.albums.map(item => {
                    return (
                        <Grid item key={item.id} xs={4}>
                            <Grid item>
                                <Card sx={{borderRadius: 2,}}>
                                    <Grid container direction={"row"} alignItems={"center"}>
                                        <Grid item xs={10}>
                                            <CardActionArea sx={{paddingTop: 0, paddingBottom: 0}}>
                                                <Grid container direction={"row"} alignItems={"center"}>
                                                    <Grid item xs={2}>
                                                        <CardMedia
                                                            component="img"
                                                            image={AlbumImage}
                                                            alt="green iguana"
                                                        />
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography ml={2} variant={"h5"}
                                                        >
                                                            {item.genre}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </CardActionArea>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <IconButton color="secondary" aria-label="add an alarm">
                                                <PlayArrowIcon fontSize={"large"}/>
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </Card>
                            </Grid>
                        </Grid>
                    )
                })}
            </Grid>
        </Box>
    );
};

export default RecentArtists;