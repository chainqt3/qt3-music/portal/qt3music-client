import React, {useState} from 'react';
import Box from "@mui/material/Box";
import useTheme from "@mui/material/styles/useTheme";
import RecentArtists from "./RecentArtists";
import YourShows from "./YourShows";
import YourFavoriteArtist from "./YourFavoriteArtist";
import RecentlyPLayed from "./RecentlyPLayed";
import {Button} from "@mui/material";
import { fetchGatewayTest, fetchUsers } from '../../../root/RootApi';
import {useKeycloak} from "@react-keycloak/web";
import {User} from "../../../root/RootModel";

const Home = () => {
  const dawdaw = () => {
    return 1 +1;
  }
  console.log(dawdaw())
    return (
        <Box>
            <RecentArtists/>
            <YourShows/>
            <YourFavoriteArtist/>
            <RecentlyPLayed/>
        </Box>
    )
};

export default Home;
