import React, {useContext} from 'react';
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {CardActionArea, CardMedia, Grid} from "@mui/material";
import AlbumImage from "../../../assets/images/billing-background-card.png";
import {HomeViewContext} from "../HomeViewContext";

const YourShows = () => {
    const homeViewModel = useContext(HomeViewContext);
    return (
        <Box>
            <Grid container direction={"column"} spacing={3}>
                <Grid item>
                    <Typography mt={5} fontWeight={"bold"} sx={{color: "white"}} variant={"h5"}>Your Favorite
                        Artist</Typography>
                </Grid>
                <Grid item>
                    <Grid container direction={"row"} spacing={1}>
                        {homeViewModel.yourShows.map(item => {
                            return (
                                <Grid item key={item.id} xs={6} md={2}>
                                    <CardActionArea>
                                        <Grid container direction={"column"}>
                                            <Grid item border={"solid"}>
                                                <CardMedia
                                                    component="img"
                                                    image={AlbumImage}
                                                    alt="green iguana"
                                                    sx={{height: 100}}
                                                />
                                            </Grid>
                                            <Grid item>
                                                <Typography variant={"h5"}>{item.title}</Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography variant={"h5"}>{item.subTitle}</Typography>
                                            </Grid>
                                        </Grid>
                                    </CardActionArea>
                                </Grid>
                            )
                        })}
                    </Grid>
                </Grid>
            </Grid>

        </Box>
    );
};

export default YourShows;