import React from 'react';
import {LikedSongsViewProvider} from "./LikedSongsViewContext";
import LikedSongs from "./components/LikedSongs";
import Page from "../../root/components/Page";

const LikedSongsView = () => {
    return (
        <Page title={"LikedSongs | ChainQT3"}>
            <LikedSongsViewProvider>
                <LikedSongs/>
            </LikedSongsViewProvider>
        </Page>
    );
};

export default LikedSongsView;