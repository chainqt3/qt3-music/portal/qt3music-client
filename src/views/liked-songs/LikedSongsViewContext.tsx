import React, {createContext, useEffect, useState} from 'react';
import {RootModel} from "../../root/RootModel";

export const LikedSongsViewContext = createContext<RootModel | undefined>(undefined);

type Props = {
    children: JSX.Element,
};

export const LikedSongsViewProvider = ({children}: Props) => {
    const [LikedSongsViewModel, setLikedSongsViewModel] = useState<RootModel>()

    useEffect(() => {
        // const tmpArray = {
        //     name: "",
        //     sideMenuContent: [{id: "string", title: "string", subContent: [{id: "string", title: "string",}]}],
        //     messages: [{id: "string", title: "string", content: "string"}],
        //     notifyMessages: [{id: "string", date: "string", time: "string", title: "string", description: "string", author: "string"}],
        //     playlist: [{id: 1, title: "string", checked: false}]
        // }
        // setLikedSongsViewModel(tmpArray)
    }, [])

    if (!LikedSongsViewModel) return null

    return (
        <LikedSongsViewContext.Provider value={LikedSongsViewModel}>
            {children}
        </LikedSongsViewContext.Provider>
    )
}
