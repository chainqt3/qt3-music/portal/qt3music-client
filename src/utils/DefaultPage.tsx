import React from 'react';
import chainDark from "../assets/images/logos/chaincue-logo-crop1-dark.png";

const DefaultPage = () => {
  return (
    <div style={{
      display: "flex",
      justifyContent: "center",
      height: "100vh",
      background: "linear-gradient(0, #3F3F46, #6A717F)"
    }}>
      <img src={chainDark} alt={""} style={{
        height: 100,
        marginTop: 161
      }}/>
    </div>
  );
};

export default DefaultPage;
