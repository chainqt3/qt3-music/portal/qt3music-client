import React from 'react';
import {CircularProgress, Fab} from "@mui/material";
import SecurityIcon from '@mui/icons-material/Security';
import {blue, green} from "@mui/material/colors";
import {Box} from "@mui/system";

export default function LoadingProgress() {
    return (
        <Box m={1}>
            <Fab
                aria-label="save"
                color="primary"
            >
                <SecurityIcon/>
            </Fab>
            <CircularProgress
                size={68}
                sx={{
                    color: blue[500],
                    position: 'absolute',
                    top: 2,
                    left: 2,
                    zIndex: 1,
                }}
            />
        </Box>
    )
}
